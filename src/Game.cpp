/*
 * Game.cpp
 *
 *  Created on: Dec. 11, 2020
 *      Author: Matthew Virdaeus
 */

#include "../include/Game.h"

#include <exception>
#include <iostream>
#include <string>

using retrofrog::Game;

Game::Game()
: running_(false)
, window_(NULL)
{

	if(SDL_Init(SDL_INIT_VIDEO) < 0) {
		std::string errorMsg = "Failed to initialize video. SDL error: " + std::string(SDL_GetError());
		throw std::runtime_error(errorMsg);
	}
}

Game::~Game() {
	if(window_ != NULL) {
		SDL_DestroyWindow(window_);
	}

	SDL_Quit();
}

void Game::run(const std::string &title, unsigned int width,
		unsigned int height) {

	window_ = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
			width, height, SDL_WINDOW_SHOWN);
	if(window_ == NULL) {
		std::string errorMsg = "Failed to create window. SDL error: " + std::string(SDL_GetError());
		std::cerr << errorMsg;
		return;
	}

	running_ = true;
	SDL_Event event;

	while(running_) {

		while(SDL_PollEvent(&event) != 0) {

			switch(event.type) {
			case SDL_QUIT:
				running_ = false;
				break;
			}
		}
	}
}

void Game::pushState(std::unique_ptr<retrofrog::GameState> state) {
	states_.push_back(std::move(state));
}

void Game::popState() {
	if(!states_.empty()) {
		states_.pop_back();
	}
}

retrofrog::GameState* Game::backState() {
	return states_.back().get();
}
