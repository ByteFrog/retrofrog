/*
 * Game.h
 *
 *  Created on: Dec. 11, 2020
 *      Author: Matthew Virdaeus
 */

#pragma once

#include "GameState.h"

#include <SDL2/SDL.h>
#include <memory>
#include <string>
#include <vector>

namespace retrofrog {

class Game {
public:
	Game();
	virtual ~Game();

	void run(const std::string& title, unsigned int width, unsigned int height);

	void pushState(std::unique_ptr<retrofrog::GameState> state);
	void popState();
	retrofrog::GameState* backState();

private:
	bool running_;
	SDL_Window* window_;
	std::vector<std::unique_ptr<retrofrog::GameState>> states_;
};

}
