/*
 * GameState.h
 *
 *  Created on: Dec. 15, 2020
 *      Author: Matthew Virdaeus
 */

#pragma once

namespace retrofrog {

class Game;

class GameState {
public:
	GameState(Game* parent) : parent_(parent), paused_(false), hidden_(false) { };
	virtual ~GameState();

	virtual void update() = 0;
	virtual void render() = 0;

	bool paused() { return paused_; }
	void pause(auto paused) { paused_ = paused; }

	bool hidden() { return hidden_; }
	void hide(auto hide) { hidden_ = hide; }

private:
	Game* parent_;
	bool paused_;
	bool hidden_;
};

}
